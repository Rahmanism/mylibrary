unit untData;

interface

uses
  SysUtils, Classes, DB, DBTables;

type
  TDataModule1 = class(TDataModule)
    dtsCDs: TDataSource;
    dtsBooks: TDataSource;
    dtsFriends: TDataSource;
    qryCDs: TQuery;
    qryBooks: TQuery;
    qryFriends: TQuery;
    dtsBorrows: TDataSource;
    qryBorrows: TQuery;
    qryTemp: TQuery;
    procedure qryCDsAfterScroll(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DataModule1: TDataModule1;
  PIC_PATH, CD_DB: String;
  FirstTimeRun: Boolean;

implementation

uses untMain, untCDs;

{$R *.dfm}

procedure TDataModule1.qryCDsAfterScroll(DataSet: TDataSet);
begin
  if FirstTimeRun then
    FirstTimeRun := False
  else
    frmCDs.RecRefresh;
end;

procedure TDataModule1.DataModuleCreate(Sender: TObject);
begin
  PIC_PATH := GetCurrentDir + '\..\pData\pics\';
  CD_DB := GetCurrentDir + '\..\pData\CDs.db';
  qryCDs.SQL.Clear;
  qryCDs.SQL.Add('select * from "' +
    CD_DB + '"');
  FirstTimeRun := True;
  qryCDs.Active := True;
end;

end.
