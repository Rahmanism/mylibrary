object frmMain: TfrmMain
  Left = 192
  Top = 114
  Width = 750
  Height = 480
  BiDiMode = bdRightToLeft
  Caption = #1603#1578#1575#1576#1582#1575#1606#1607
  Color = clBtnFace
  Font.Charset = ARABIC_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIForm
  Menu = MainMenu1
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poDesktopCenter
  ShowHint = True
  PixelsPerInch = 96
  TextHeight = 13
  object MainMenu1: TMainMenu
    Left = 8
    Top = 8
    object mmFile: TMenuItem
      Caption = #1601#1575#1610#1604
      object smFriends: TMenuItem
        Caption = #1583#1608#1587#1578#1575#1606
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object smCDs: TMenuItem
        Caption = #1587#1610#8204#1583#1610
        OnClick = smCDsClick
      end
      object smBooks: TMenuItem
        Caption = #1603#1578#1575#1576
      end
      object N6: TMenuItem
        Caption = '-'
      end
      object smExit: TMenuItem
        Caption = #1582#1585#1608#1580
        OnClick = smExitClick
      end
    end
  end
  object XPManifest1: TXPManifest
    Left = 40
    Top = 8
  end
end
