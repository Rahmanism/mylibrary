unit untPicShow;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TfrmPicShow = class(TForm)
    Image1: TImage;
    Panel1: TPanel;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure Image1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPicShow: TfrmPicShow;

implementation

{$R *.dfm}

procedure TfrmPicShow.FormKeyPress(Sender: TObject; var Key: Char);
begin
//  if Key in [#27,#13,#32] then
    Close;
end;

procedure TfrmPicShow.Image1Click(Sender: TObject);
begin
  Close;
end;

end.
