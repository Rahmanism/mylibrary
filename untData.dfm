object DataModule1: TDataModule1
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 192
  Top = 112
  Height = 212
  Width = 231
  object dtsCDs: TDataSource
    DataSet = qryCDs
    Left = 16
    Top = 8
  end
  object dtsBooks: TDataSource
    DataSet = qryBooks
    Left = 64
    Top = 8
  end
  object dtsFriends: TDataSource
    DataSet = qryFriends
    Left = 112
    Top = 8
  end
  object qryCDs: TQuery
    AfterScroll = qryCDsAfterScroll
    Left = 16
    Top = 56
  end
  object qryBooks: TQuery
    SQL.Strings = (
      'select * from '#39'..\pData\Books.db'#39
      '')
    Left = 64
    Top = 56
  end
  object qryFriends: TQuery
    SQL.Strings = (
      'select * from '#39'..\pData\Friends.db'#39)
    Left = 112
    Top = 56
  end
  object dtsBorrows: TDataSource
    DataSet = qryBorrows
    Left = 168
    Top = 8
  end
  object qryBorrows: TQuery
    SQL.Strings = (
      'select * from '#39'..\pData\Borrows.db'#39)
    Left = 168
    Top = 56
  end
  object qryTemp: TQuery
    Left = 16
    Top = 112
  end
end
