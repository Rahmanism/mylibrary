program MyLibrary;

uses
  Forms,
  untMain in 'untMain.pas' {frmMain},
  untCDs in 'untCDs.pas' {frmCDs},
  untData in 'untData.pas' {DataModule1: TDataModule},
  untPicShow in 'untPicShow.pas' {frmPicShow},
  untSearch in 'untSearch.pas' {frmSearch};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := '��������';
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TDataModule1, DataModule1);
  Application.CreateForm(TfrmCDs, frmCDs);
  Application.CreateForm(TfrmPicShow, frmPicShow);
  Application.CreateForm(TfrmSearch, frmSearch);
  Application.Run;
end.
