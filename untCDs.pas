unit untCDs;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls, ExtCtrls, ToolWin, ComCtrls, DBCtrls,
  Buttons, ImgList, jpeg, ExtDlgs, IWControl, IWCompButton, Menus;

type
  TfrmCDs = class(TForm)
    grbInfo: TGroupBox;
    lblGrp: TLabel;
    lbeCID: TLabeledEdit;
    lbeGNo: TLabeledEdit;
    cmbGrp: TComboBox;
    lbeTitle: TLabeledEdit;
    lbeProducer: TLabeledEdit;
    lbeBDday: TLabeledEdit;
    lbePrice: TLabeledEdit;
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    tbrCDs: TToolBar;
    imlToolbar: TImageList;
    tbnLast: TToolButton;
    tbnNext: TToolButton;
    tbnPre: TToolButton;
    tbnFirst: TToolButton;
    tbnAdd: TToolButton;
    tbnSep1: TToolButton;
    tbnDel: TToolButton;
    tbnSep2: TToolButton;
    tbnSave: TToolButton;
    tbnCancel: TToolButton;
    spbToday: TSpeedButton;
    memComm: TMemo;
    chbOriginal: TCheckBox;
    chbExist: TCheckBox;
    chbDVD: TCheckBox;
    chbDeleted: TCheckBox;
    lblComm: TLabel;
    grbPic: TGroupBox;
    imgPic: TImage;
    OpenPictureDialog1: TOpenPictureDialog;
    edtBDmon: TEdit;
    edtBDyear: TEdit;
    ppmPic: TPopupMenu;
    ppmSavePicture: TMenuItem;
    ppmShowPicture: TMenuItem;
    ppmDeletePicture: TMenuItem;
    tbnSep3: TToolButton;
    tbnDeldImages: TToolButton;
    tbnSep4: TToolButton;
    tbnSearch: TToolButton;
    procedure tbnLastClick(Sender: TObject);
    procedure tbnNextClick(Sender: TObject);
    procedure tbnPreClick(Sender: TObject);
    procedure tbnFirstClick(Sender: TObject);
    procedure tbnAddClick(Sender: TObject);
    procedure tbnDelClick(Sender: TObject);
    procedure tbnSaveClick(Sender: TObject);
    procedure tbnCancelClick(Sender: TObject);
    procedure PicRefresh;
    procedure RecRefresh;
    procedure imgPicClick(Sender: TObject);
    procedure chbDeletedClick(Sender: TObject);
    procedure chbExistClick(Sender: TObject);
    procedure spbTodayClick(Sender: TObject);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ppmShowPictureClick(Sender: TObject);
    procedure ppmSavePictureClick(Sender: TObject);
    procedure ppmDeletePictureClick(Sender: TObject);
    procedure tbnDeldImagesClick(Sender: TObject);
    procedure cmbGrpChange(Sender: TObject);
    procedure tbnSearchClick(Sender: TObject);
  private
    procedure AppendRecord;
    procedure DeleteRecord;
    procedure CancelRecord;
    procedure UpdateRecord;
    procedure MemoGotoFirst;
    procedure memRefresh;
    function MyBoolToStr(b: Boolean): String;
    procedure ClearCells;
    procedure InsertRecord;
    procedure SavePicture(blnRefresh: Boolean);
    procedure ShowPicture;
    procedure DeletePicture(blnRefresh: Boolean);
    function NewPicPathStr(d: Boolean): String;
    function LastGNo: Integer;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCDs: TfrmCDs;
  PicPath: String;
  NowCID: Integer;
  AppRecord: Boolean;

implementation

uses untMain, untData, untPicShow, DBTables, DateUtils, untSearch;

{$R *.dfm}

procedure TfrmCDs.tbnLastClick(Sender: TObject);
begin
  DataModule1.qryCDs.Last;
end;

procedure TfrmCDs.tbnNextClick(Sender: TObject);
begin
  DataModule1.qryCDs.Next;
end;

procedure TfrmCDs.tbnPreClick(Sender: TObject);
begin
  DataModule1.qryCDs.Prior;
end;

procedure TfrmCDs.tbnFirstClick(Sender: TObject);
begin
  DataModule1.qryCDs.First;
end;

procedure TfrmCDs.tbnAddClick(Sender: TObject);
begin
  AppendRecord;
end;

procedure TfrmCDs.ClearCells;
begin
  lbeCID.Text := '';
  lbeGNo.Text := '';
  cmbGrp.ItemIndex := 0;
  lbeTitle.Text := '';
  lbeProducer.Text := '';
  lbeBDday.Text := '';
  edtBDmon.Text := '';
  edtBDyear.Text := '';
  lbePrice.Text := '';
  chbOriginal.Checked := False;
  chbExist.Checked := True;
  chbDVD.Checked := False;
  chbDeleted.Checked := False;
  memComm.Lines.Clear;
  PicPath := PIC_PATH + 'nopic.bmp';
  imgPic.Picture.LoadFromFile(PicPath);
end;

procedure TfrmCDs.AppendRecord;
begin
  DataModule1.qryCDs.Last;
  NowCID := DataModule1.qryCDs['CID'] + 1;
  ClearCells;
  lbeCID.Text := IntToStr(NowCID);
  lbeGNo.Text := IntToStr(LastGNo + 1);
  AppRecord := True;
  cmbGrp.SetFocus;
end;

procedure TfrmCDs.tbnDelClick(Sender: TObject);
begin
  DeleteRecord;
end;

procedure TfrmCDs.DeleteRecord;
var
  tmpRecNo: Integer;
begin
  tmpRecNo := DataModule1.qryCDs.RecNo;
  if MessageBox(Self.Handle, '            ��� ������Ͽ', '��� �����',
    MB_YESNO) = IDYES then
  begin
    with DataModule1 do
    begin
      qryCDs.Active := False;
      qryTemp.SQl.Clear;
      qryTemp.SQL.Add(
        'delete from ' + QuotedStr(CD_DB) +
        ' where CID = ' + IntToStr(NowCID));
      DeletePicture(False);
      qryTemp.ExecSQL;
      qryCDs.Active := True;
      qryCDs.MoveBy(tmpRecNo - 1);
    end;
  end;
end;

procedure TfrmCDs.tbnSaveClick(Sender: TObject);
begin
  if AppRecord then
    InsertRecord
  else
    UpdateRecord;
end;

function TfrmCDs.MyBoolToStr(b: Boolean): String;
begin
  if b then
    Result := 'True'
  else
    Result := 'False';
end;

function TfrmCDs.LastGNo: Integer;
begin
  with DataModule1 do
  begin
    qryTemp.SQL.Clear;
    qryTemp.SQL.Add('select GNo from "' + CD_DB + '" ' +
      'where Grp = ' + IntToStr(cmbGrp.ItemIndex + 1));
    qryTemp.Active := True;
    qryTemp.Last;
    Result := StrToInt(qryTemp['GNo']);
  end;
end;

procedure TfrmCDs.InsertRecord;
var
  strPrice, strBDate: String;
  tmpCID: Integer;
begin
  tmpCID := StrToInt(lbeCID.Text);
  if lbePrice.Text = '' then
    strPrice := 'null'
  else
    strPrice := lbePrice.Text;
  if (lbeBDday.Text = '') or (edtBDmon.Text = '')
    or (edtBDyear.Text = '') then
    strBDate := 'null'
  else
    strBDate := '"' + edtBDmon.Text + '/' +
      lbeBDday.Text + '/' + edtBDyear.Text + '"';
  with DataModule1 do
  begin
    qryCDs.Active := False;
    qryTemp.SQL.Clear;
    qryTemp.SQL.Add('insert into "' + CD_DB + '" ' +
      '(CID, GNo, Grp, Title, Producer, Original, BDate, ' +
      'Price, DVD, Exist, Deleted, Comment) values (' +
      lbeCID.Text + ', ' + lbeGNo.Text + ', ' +
      IntToStr(cmbGrp.ItemIndex + 1) + ', ' +
      QuotedStr(lbeTitle.Text) + ', ' +
      QuotedStr(lbeProducer.Text) + ', ' +
      MyBoolToStr(chbOriginal.Checked) + ', ' +
      strPrice + ', ' + strBDate + ', ' +
      MyBoolToStr(chbDVD.Checked) + ', ' +
      MyBoolToStr(chbExist.Checked) + ', ' +
      MyBoolToStr(chbDeleted.Checked) + ', ' +
      QuotedStr(memComm.Lines.Text) +  ');');
/////////////////////////////////////
    SavePicture(False);
    qryTemp.ExecSQL;
    qryCDs.Active := True;
    qryCDs.Locate('CID', tmpCID, []);
    AppRecord := False;
  end;
end;

procedure TfrmCDs.SavePicture(blnRefresh: Boolean);
var
  InsertPic, NewPicPath: String;
begin
  if OpenPictureDialog1.Execute then
  begin
    NewPicPath := NewPicPathStr(False);
    if NewPicPath = PicPath then
      DeletePicture(False);
    InsertPic := OpenPictureDialog1.FileName;
    CopyFile(PChar(InsertPic), PChar(NewPicPath),  True);
  end;
  if blnRefresh then
    PicRefresh;
end;

function TfrmCDs.NewPicPathStr(d: Boolean): String;
begin
  if d then
    Result := PIC_PATH + 'd' +
      lbeCID.Text + '-' + lbeGNo.Text + '-' +
      IntToStr(cmbGrp.ItemIndex + 1) + '.jpg'
  else
    Result := PIC_PATH +
      lbeCID.Text + '-' + lbeGNo.Text + '-' +
      IntToStr(cmbGrp.ItemIndex + 1) + '.jpg';
end;

procedure TfrmCDs.UpdateRecord;
var
  NewPicPath, strPrice, strBDate: String;
  tmpCID: Integer;
begin
  tmpCID := StrToInt(lbeCID.Text);
  if lbePrice.Text = '' then
    strPrice := 'null'
  else
    strPrice := lbePrice.Text;
  if (lbeBDday.Text = '') or (edtBDmon.Text = '')
    or (edtBDyear.Text = '') then
    strBDate := 'null'
  else
    strBDate := '"' + edtBDmon.Text + '/' +
      lbeBDday.Text + '/' + edtBDyear.Text + '"';
  with DataModule1 do
  begin
    qryCDs.Active := False;
    qryTemp.SQL.Clear;
    qryTemp.SQL.Add('update "' + CD_DB + '" SET ' +
      'CID = ' + lbeCID.Text + ', ' +
      'GNo = ' + lbeGNo.Text + ', ' +
      'Grp = ' + IntToStr(cmbGrp.ItemIndex + 1) + ', ' +
      'Title = ' + QuotedStr(lbeTitle.Text) + ', ' +
      'Producer = ' + QuotedStr(lbeProducer.Text) + ', ' +
      'Original = ' + MyBoolToStr(chbOriginal.Checked) + ', ' +
      'Price = ' + strPrice + ', ' +
      'BDate = ' + strBDate + ', ' +
      'DVD = ' + MyBoolToStr(chbDVD.Checked) + ', ' +
      'Exist = ' + MyBoolToStr(chbExist.Checked) + ', ' +
      'Deleted = ' + MyBoolToStr(chbDeleted.Checked) + ', ' +
      'Comment = ' + QuotedStr(memComm.Lines.Text) +  ' ' +
      'WHERE CID = ' + IntToStr(NowCID) + ';');
    if (FileExists(PicPath)) and (not (PicPath = PIC_PATH + 'nopic.bmp')) then
    begin
      NewPicPath := NewPicPathStr(False);
      RenameFile(PicPath, NewPicPath);
    end;
    qryTemp.ExecSQL;
    qryCDs.Active := True;
    qryCDs.Locate('CID', tmpCID, []);
  end;
end;

procedure TfrmCDs.tbnCancelClick(Sender: TObject);
begin
  CancelRecord;
end;

procedure TfrmCDs.CancelRecord;
begin
  RecRefresh;
end;

procedure TfrmCDs.RecRefresh;
var
  yyyy, mm, dd: Word;
begin
  PicRefresh;
  with DataModule1 do
  begin
    NowCID := qryCDs['CID'];
    lbeCID.Text := IntToStr(NowCID);
    lbeGNo.Text := IntToStr(qryCDs['GNo']);
    case qryCDs['Grp'] of
      1: cmbGrp.ItemIndex := 0;
      2: cmbGrp.ItemIndex := 1;
      3: cmbGrp.ItemIndex := 2;
    end;
    if not (qryCDs['Title'] = Null) then
      lbeTitle.Text := qryCDs['Title']
    else
      lbeTitle.Text := '';
    if not (qryCDs['Producer'] = Null) then
      lbeProducer.Text := qryCDs['Producer']
    else
      lbeProducer.Text := '';
    if not (qryCDs['BDate'] = Null) then
    begin
      DecodeDate(qryCDs['BDate'], yyyy, mm, dd);
      lbeBDday.Text := IntToStr(dd);
      edtBDmon.Text := IntToStr(mm);
      edtBDyear.Text := IntToStr(yyyy);
    end
    else
    begin
      lbeBDday.Text := '';
      edtBDmon.Text := '';
      edtBDyear.Text := '';
    end;
    if not (qryCDs['Price'] = Null) then
      lbePrice.Text := IntToStr(qryCDs['Price'])
    else
      lbePrice.Text := '';
    if qryCDs['Original'] then
      chbOriginal.Checked := True
    else
      chbOriginal.Checked := False;
    if qryCDs['DVD'] then
      chbDVD.Checked := True
    else
      chbDVD.Checked := False;
    if qryCDs['Exist'] then
      chbExist.Checked := True
    else
      chbExist.Checked := False;
    if qryCDs['Deleted'] then
      chbDeleted.Checked := True
    else
      chbDeleted.Checked := False;
    memRefresh;
  end;
end;

procedure TfrmCDs.memRefresh;
begin
  memComm.Lines.Clear;
  memComm.Lines.Add(DataModule1.qryCDs['Comment']);
  MemoGotoFirst;
end;

procedure TfrmCDs.MemoGotoFirst;
begin
  memComm.SelStart := 0;
  memComm.SelLength := 0;
end;

procedure TfrmCDs.PicRefresh;
begin
  PicPath := PIC_PATH +
    IntToStr(DataModule1.qryCDs['CID']) + '-' +
    IntToStr(DataModule1.qryCDs['GNo']) + '-' +
    IntToStr(DataModule1.qryCDs['Grp']) + '.jpg';
  if not FileExists(PicPath) then
    PicPath := PIC_PATH + 'nopic.bmp';
  imgPic.Picture.LoadFromFile(PicPath);
end;

procedure TfrmCDs.imgPicClick(Sender: TObject);
begin
  ShowPicture;
end;

procedure TfrmCDs.ShowPicture;
begin
  with frmPicShow do
  begin
    Image1.Picture.LoadFromFile(PicPath);
    Image1.AutoSize := True;
    AutoSize := False;
    if (Image1.Height > 766) and (Image1.Width >1022) then
    begin
      Panel1.AutoSize := False;
      Image1.AutoSize := False;
      Image1.Width := 1019;
      Image1.Height := 762;
      Panel1.AutoSize := True;
    end;
    AutoSize := True;
    Position := poScreenCenter;
    ShowModal;
  end;
end;

procedure TfrmCDs.chbDeletedClick(Sender: TObject);
begin
  if chbDeleted.Checked then
    chbExist.Checked := False;
end;

procedure TfrmCDs.chbExistClick(Sender: TObject);
begin
  if chbExist.Checked then
    chbDeleted.Checked := False;
end;

procedure TfrmCDs.spbTodayClick(Sender: TObject);
var
  yyyy, mm, dd: Word;
begin
  DecodeDate(Date, yyyy, mm, dd);
  lbeBDday.Text := IntToStr(dd);
  edtBDmon.Text := IntToStr(mm);
  edtBDyear.Text := IntToStr(yyyy);
end;

procedure TfrmCDs.DBGrid1TitleClick(Column: TColumn);
begin
  with DataModule1 do
  begin
    qryCDs.SQL.Clear;
    qryCDs.SQL.Add('select * from "' +
      CD_DB + '" ORDER BY ' + Column.FieldName);
    qryCDs.Active := True;
  end;
end;

procedure TfrmCDs.FormCreate(Sender: TObject);
begin
  AppRecord := False;
end;

procedure TfrmCDs.FormShow(Sender: TObject);
begin
  DataModule1.qryCDs.Active := False;
  DataModule1.qryCDs.Active := True;
end;

procedure TfrmCDs.ppmShowPictureClick(Sender: TObject);
begin
  ShowPicture;
end;

procedure TfrmCDs.ppmSavePictureClick(Sender: TObject);
begin
  SavePicture(True);
end;

procedure TfrmCDs.DeletePicture(blnRefresh: Boolean);
var
  NewPicPath: String;
begin
  if (FileExists(PicPath)) and (not (PicPath = PIC_PATH + 'nopic.bmp')) then
  begin
    NewPicPath := NewPicPathStr(True);
    if FileExists(NewPicPath) then
      DeleteFile(NewPicPath);
    RenameFile(PicPath, NewPicPath);
  end;
  if blnRefresh then
    PicRefresh;
end;

procedure TfrmCDs.ppmDeletePictureClick(Sender: TObject);
begin
  if MessageBox(Self.Handle, '            ��� ������Ͽ', '��� �����',
    MB_YESNO) = IDYES then
    DeletePicture(True);
end;

procedure TfrmCDs.tbnDeldImagesClick(Sender: TObject);
var
  sr: TSearchRec;
begin
  if MessageBox(Self.Handle, '            ��� ������Ͽ', '��� ������ �����',
    MB_YESNO) = IDYES then
  begin
    if FindFirst(PIC_PATH + 'd*', faAnyFile, sr) = 0 then
    begin
      repeat
        DeleteFile(PIC_PATH + sr.Name);
      until FindNext(sr) <> 0;
      FindClose(sr);
    end;
    ShowMessage('��� ������ ����� ��� ����.');
  end;
end;

procedure TfrmCDs.cmbGrpChange(Sender: TObject);
begin
  lbeGNo.Text := IntToStr(LastGNo + 1);
end;

procedure TfrmCDs.tbnSearchClick(Sender: TObject);
begin
  frmSearch.ShowModal;
end;

end.
