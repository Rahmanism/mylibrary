unit untMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, DB, DBTables, XPMan;

type
  TfrmMain = class(TForm)
    MainMenu1: TMainMenu;
    mmFile: TMenuItem;
    smCDs: TMenuItem;
    smBooks: TMenuItem;
    smFriends: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    smExit: TMenuItem;
    XPManifest1: TXPManifest;
    procedure smCDsClick(Sender: TObject);
    procedure smExitClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses untCDs;

{$R *.dfm}

procedure TfrmMain.smCDsClick(Sender: TObject);
begin
  frmCDs.Show;
  frmCDs.WindowState := wsNormal;
end;

procedure TfrmMain.smExitClick(Sender: TObject);
begin
  Application.Terminate;
end;

end.
