unit untSearch;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TfrmSearch = class(TForm)
    lbesCID: TLabeledEdit;
    btnSearch: TButton;
    procedure btnSearchClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSearch: TfrmSearch;

implementation

uses untData;

{$R *.dfm}

procedure TfrmSearch.btnSearchClick(Sender: TObject);
begin
  if lbesCID.Text='' then
    exit;
  with DataModule1 do
  begin
    qryCDs.Active := False;
    qryCDs.SQL.Clear;
    qryCDs.SQL.Add('select * from "' + CD_DB + '" c where c.cid='
      + lbesCID.Text);
    qryCDs.Active := True;
  end;
end;

end.
